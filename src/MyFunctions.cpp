#include <math.h>
#include <fstream>
#include <MyFunctions.hpp>

using namespace std;

int Begin(int(&arr)[100][100], int& I, int& J)
{
    //�������� ���������� ����� � ���������� ������� � arr[][]. 
    int x;
    ifstream fin("in.txt");

    fin >> x;
    I = x;
    fin >> x;
    J = x;
    if ((I > 100 || J > 100) || (I < 1 || J < 1))
        return 0;
    for (int i = 0; i < I; i++)
    {

        for (int j = 0; j < J; j++)
        {
            fin >> x;
            arr[i][j] = x;
        }
    }
    return 1;
}

//�������� �� ������� �����
bool SimpleCount(int count)
{
    if (count < 2)
        return false;
    for (int d = 2; d <= sqrt(count); d++)
        if (count % d == 0)
            return false;
    return true;
}

//����� ���������� ������� ������� � ������� ����� � ���� ��������.
int Find(int(&arr)[100][100], int& I, bool& flag, int j, int i, bool& Simple_Flag)
{
    int n = 0;
    int q;
    int w;
    int Find_i = 0;
    Simple_Flag = 0;
    do
    {
        if (SimpleCount(arr[Find_i][i]) == 1 || SimpleCount(arr[Find_i][j]) == 1)
            Simple_Flag = 1;
        q = arr[Find_i][i];
        w = arr[Find_i][j];
        if (arr[Find_i][i] != arr[Find_i][j])
        {
            flag = 1;
            return 0;
        }
        Find_i++;
        n++;
        if (n == I && flag == 0)
        {
            return 1;
        }
        else if (n == I && flag == 1)
            return 0;
    } while (n != I);
}

//�������� �� ���������� ������� ������ � �������.
int FirstProcessing(int(&arr)[100][100], int& I, int& J)
{
    bool Simple_Flag = 0;
    bool flag = 0;
    for (int i = 0; i < I; i++)
    {
        for (int j = i + 1; j < J; j++)
        {
            if (Find(arr, I, flag, j, i, Simple_Flag) == 1 && Simple_Flag == 1)
            {
                return 1;
            }
            flag = 0;
        }
    }
    return 0;
}

//����� �������� ������� ��� �������������� ����� �� ����������� ����� ������� �����.
int MatrixProcessing(int(&arr)[100][100], int& I, int& J)
{
    bool flag = 0;
    for (int i = 0; i < I - 1; i++)
    {
        int Sum_1 = 0;
        int Sum_2 = 0;
        for (int j = 0; j < J; j++)
        {
            Sum_1 += abs(arr[i][j]);
            if (J > 1)
                Sum_2 += abs(arr[i + 1][j]);
        }
        if (Sum_1 > Sum_2)
        {
            flag = 1;
            for (int j = 0; j < J; j++)
            {
                if (i + 1 <= I)
                    swap(arr[i][j], arr[i + 1][j]);
                else
                    break;
            }
        }
    }
    if (flag == 1)
        return 1;
    else
        return 0;
}

//���� ��������� ������� � ����.
void MatrixOutput(int(&arr)[100][100], int& I, int& J)
{
    ofstream fout("out.txt");

    for (int i = 0; i < I; i++)
    {
        for (int j = 0; j < J; j++)
            fout << arr[i][j] << "\t";
        fout << "\n";
    }
}