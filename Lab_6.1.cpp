﻿//Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. Если в матрице есть два одинаковых столбца и есть хотя бы один 
// элемент, абсолютная величина которого - простое число, упорядочить строки матрицы по неубыванию суммы модулей элементов.

#include <iostream>
#include <fstream>
#include <windows.h>
#include <MyFunctions.hpp>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    int arr[100][100];
    int I, J;
    if (Begin(arr, I, J) == 0)
    {
        cout << "Введена матрица большая 100x100! " << endl;
        return 0;
    }
    if (FirstProcessing(arr, I, J) == 1)
        while (MatrixProcessing(arr, I, J))
        {
            MatrixProcessing(arr, I, J);
        }
    MatrixOutput(arr, I, J);
    cout << "Всё прошло успешно, итоговая матрица внесена в файл!" << endl;
    return 1;
}
